package ru.will0376.windcheck.server.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheckbridge.events.req.RequestCrashPlayerEvent;
import ru.will0376.windcheckbridge.utils.AbstractCommand;
import ru.will0376.windcheckbridge.utils.Token;

import java.util.Arrays;
import java.util.List;

@GradleSideOnly(GradleSide.SERVER)
public class CrashCommand extends AbstractCommand {
	public CrashCommand() {
		super("crash");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("player").setRequired().setHasArg().build(),
				getArgBuilder("force").setHide().build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().args(args).adminNick(sender.getName()).build();
		if (!WindCheck.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);

		if (WindCheck.aVersion.findPlayer(line.getOptionValue("player"), line.hasOption("force")) == null) {
			return build.setCanceled("player not found!");
		}

		RequestCrashPlayerEvent player = new RequestCrashPlayerEvent(line.getOptionValue("player"), sender.getName(),
				sender instanceof MinecraftServer ? Token.Requester.Console : Token.Requester.Admin);
		WindCheck.aVersion.sendEvent(player);
		return build;
	}

	@Override
	public List<String> getTabList(String[] args, ICommandSender sender) {
		if (args.length == 3 && args[1].contains("player")) {
			return CommandBase.getListOfStringsMatchingLastWord(args, getServer().getOnlinePlayerNames());
		}
		return super.getTabList(args, sender);
	}
}
