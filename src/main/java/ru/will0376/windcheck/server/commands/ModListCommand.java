package ru.will0376.windcheck.server.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheckbridge.events.req.RequestModListEvent;
import ru.will0376.windcheckbridge.utils.AbstractCommand;
import ru.will0376.windcheckbridge.utils.Token;

import java.util.Arrays;
import java.util.List;

@GradleSideOnly(GradleSide.SERVER)
public class ModListCommand extends AbstractCommand {
	public ModListCommand() {
		super("modList");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("player").setHasArg().setRequired().build(),
				getArgBuilder("force").setHide().build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().adminNick(sender.getName()).args(args).build();

		if (!WindCheck.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);

		if (WindCheck.aVersion.findPlayer(line.getOptionValue("player"), line.hasOption("force")) == null) {
			return build.setCanceled("player not found!");
		}
		WindCheck.aVersion.createNewToken(line.getOptionValue("player"), sender.getName(), sender instanceof MinecraftServer ?
				Token.Requester.Console : Token.Requester.Admin);
		RequestModListEvent playerEvent = new RequestModListEvent(line.getOptionValue("player"), sender.getName(),
				sender instanceof MinecraftServer ? Token.Requester.Console : Token.Requester.Admin);
		WindCheck.aVersion.sendEvent(playerEvent);
		return build;
	}

	@Override
	public List<String> getTabList(String[] args, ICommandSender sender) {
		if (args.length == 3 && args[1].contains("player")) {
			return CommandBase.getListOfStringsMatchingLastWord(args, getServer().getOnlinePlayerNames());
		}
		return super.getTabList(args, sender);
	}
}
