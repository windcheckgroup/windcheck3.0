package ru.will0376.windcheck.server.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheckbridge.utils.AbstractCommand;
import ru.will0376.windcheckbridge.utils.Module;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@GradleSideOnly(GradleSide.SERVER)
public class ToggleModuleCommand extends AbstractCommand {
	public ToggleModuleCommand() {
		super("toggleModule");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("module").setHasArg().setRequired().build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().adminNick(sender.getName()).args(args).build();

		if (!WindCheck.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);
		String modid = line.getOptionValue("module");
		Module module = WindCheck.findByModid(modid);

		if (module == null) {
			return build.setCanceled("module not found");
		}

		/*List<String> disabledModules = ServerConfig.getDisabledModules();
		if (!disabledModules.contains(modid)) disabledModules.add(modid);
		else disabledModules.remove(modid);

		ServerConfig.setModifiedDisabledModules(disabledModules);
		WindCheck.aVersion.printToSender(sender, "Done! enabled: " + disabledModules.contains(modid));*/
		return build;
	}

	@Override
	public List<String> getTabList(String[] args, ICommandSender sender) {
		if (args.length == 3 && args[1].contains("module")) {
			return CommandBase.getListOfStringsMatchingLastWord(args, WindCheck.modulesList.stream()
					.map(Module::getModid)
					.collect(Collectors.toList()));
		}
		return super.getTabList(args, sender);
	}
}
