package ru.will0376.windcheck.server.commands;

import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheck.server.ServerConfig;
import ru.will0376.windcheckbridge.utils.AbstractCommand;

import java.util.Arrays;
import java.util.List;

@GradleSideOnly(GradleSide.SERVER)
public class IPBansCommand extends AbstractCommand {
	public IPBansCommand() {
		super("ipbans");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("ban").setHasArg().build(), getArgBuilder("pardon").setHasArg().build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().args(args).adminNick(sender.getName()).build();
		if (!WindCheck.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);
		List<String> bannedIPs = ServerConfig.getBannedIPs();
		if (line.hasOption("ban")) {
			bannedIPs.add(line.getOptionValue("ban"));
		} else if (line.hasOption("pardon")) {
			bannedIPs.remove(line.getOptionValue("pardon"));
		} else {
			build.setCanceled("Use arguments");
		}
		ServerConfig.setModifiedBannedIPs(bannedIPs);
		return build;
	}
}
