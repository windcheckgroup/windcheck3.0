package ru.will0376.windcheck.server.commands;

import lombok.extern.log4j.Log4j2;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheck.server.utils.ActionLogger;
import ru.will0376.windcheckbridge.utils.AbstractCommand;
import ru.will0376.windcheckbridge.utils.SubCommands;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j2
@GradleSideOnly(GradleSide.SERVER)
public class CommandMain extends CommandBase {

	public CommandMain() {
		registerDefault(new CheckCommand(), new CrashCommand(), new ReloadCommand(), new LogCommand(), new ModListCommand(),
				/*new ToggleModuleCommand(),*/ new WebSocketCommand(), new ClassLoadedListCommand(), new IPBansCommand());
	}

	private void registerDefault(AbstractCommand... commandsIn) {
		for (AbstractCommand command : commandsIn) {
			WindCheck.aVersion.registerNewCommand(command);
			log.debug("registered command: {}", command.getCommandName());
		}
	}

	@Override
	public String getName() {
		return "windcheck";
	}

	@Override
	public String getUsage(ICommandSender iCommandSender) {
		return "";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		try {
			if (WindCheck.aVersion.senderCanUseCommand(sender, "main")) {
				if (args.length >= 1) {
					SubCommands subCommands = SubCommands.valueOf(args[0]);
					log.debug("executed {}", subCommands.getComma());
					AbstractCommand.ActionStructure execute = subCommands.getComma().execute(server, sender, args);

					if (execute.isCanceled())
						WindCheck.aVersion.printErrorToSender(sender, execute.getReason());
					else
						WindCheck.aVersion.printToSender(sender, "Performed. wait for a response from command!");

					ActionLogger.addAction(execute);

				} else {
					StringBuilder builder = new StringBuilder();
					for (SubCommands subCommands : SubCommands.values()) {
						builder.append(subCommands.name())
								.append(" :")
								.append("\n")
								.append(subCommands.getComma().getUsage())
								.append("\n")
								.append("<===>\n");
					}
					WindCheck.aVersion.printToSenderWithCase(sender, builder.toString());
				}
			} else
				WindCheck.aVersion.printErrorToSender(sender, "No rights!");
		} catch (Exception ex) {
			ex.printStackTrace();
			WindCheck.aVersion.printErrorToSender(sender, ex.getMessage());
		}
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 4;
	}

	@Override
	public List<String> getAliases() {
		ArrayList<String> al = new ArrayList<>();
		al.add("clienttweaker");
		al.add("ct");
		al.add("wc");
		al.add("wic");
		return al;
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args,
			@Nullable BlockPos targetPos) {
		if (args.length == 1)
			return getListOfStringsMatchingLastWord(args, Arrays.asList(SubCommands.values()));
		return getListOfStringsMatchingLastWord(args, SubCommands.valueOf(args[0]).getComma().getTabList(args, sender));
	}
}

