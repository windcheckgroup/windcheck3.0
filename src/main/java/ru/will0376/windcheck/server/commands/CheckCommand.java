package ru.will0376.windcheck.server.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheckbridge.events.req.RequestScreenEvent;
import ru.will0376.windcheckbridge.utils.AbstractCommand;
import ru.will0376.windcheckbridge.utils.Token;

import java.util.Arrays;
import java.util.List;

@GradleSideOnly(GradleSide.SERVER)
public class CheckCommand extends AbstractCommand {
	public CheckCommand() {
		super("check");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("player").setHasArg()
				.build(), getArgBuilder("all").build(), getArgBuilder("force").setHide().build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().adminNick(sender.getName()).args(args).build();
		if (!WindCheck.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);
		if (line.hasOption("player") || line.hasOption("all")) {
			if (line.hasOption("player") && line.hasOption("all")) {
				build.setCanceled("use only 1 argument");
			} else {
				if (line.hasOption("player")) {
					if (WindCheck.aVersion.findPlayer(line.getOptionValue("player"), line.hasOption("force")) != null) {
						WindCheck.aVersion.createNewToken(line.getOptionValue("player"), sender.getName(),
								sender instanceof MinecraftServer ? Token.Requester.Console : Token.Requester.Admin);
						RequestScreenEvent player = new RequestScreenEvent(sender.getName(), sender instanceof MinecraftServer ?
								Token.Requester.Console : Token.Requester.Admin, line.getOptionValue("player"));
						WindCheck.aVersion.sendEvent(player);
					} else {
						build.setCanceled("player not found");
					}
				} else {
					server.getServer().getPlayerList().getPlayers().forEach(e -> {
						if (!WindCheck.aVersion.checkPlayer(e.getName(), line.hasOption("force"))) {
							return;
						}
						WindCheck.aVersion.createNewToken(e.getName(), sender.getName(), sender instanceof MinecraftServer ?
								Token.Requester.Console : Token.Requester.Admin);

						RequestScreenEvent player = new RequestScreenEvent(sender.getName(), sender instanceof MinecraftServer ?
								Token.Requester.Console : Token.Requester.Admin, e.getName());
						WindCheck.aVersion.sendEvent(player);
					});
				}
				return build;
			}
		} else {
			WindCheck.aVersion.printErrorToSender(sender, "requried arg -player or -all");
		}
		return build.setCanceled();
	}

	@Override
	public List<String> getTabList(String[] args, ICommandSender sender) {
		if (args.length == 3 && args[1].contains("player")) {
			return CommandBase.getListOfStringsMatchingLastWord(args, getServer().getOnlinePlayerNames());
		}
		return super.getTabList(args, sender);
	}
}
