package ru.will0376.windcheck.server.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheckbridge.events.ReloadEvent;
import ru.will0376.windcheckbridge.utils.AbstractCommand;
import ru.will0376.windcheckbridge.utils.Module;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@GradleSideOnly(GradleSide.SERVER)
public class ReloadCommand extends AbstractCommand {
	public ReloadCommand() {
		super("reload");
	}

	@Override
	public List<Argument> getArgList() {
		return Collections.singletonList(getArgBuilder("module").setHasArg().build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().adminNick(sender.getName()).args(args).build();
		if (!WindCheck.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}
		CommandLine line = getLine(args);

		boolean module = line.hasOption("module");
		ReloadEvent player = new ReloadEvent(module, module ? WindCheck.findByModid(line.getOptionValue("module")) : null,
				sender.getName());
		WindCheck.aVersion.sendEvent(player);
		return build;
	}

	@Override
	public List<String> getTabList(String[] args, ICommandSender sender) {
		if (args.length == 3 && args[1].contains("module")) {
			return CommandBase.getListOfStringsMatchingLastWord(args, WindCheck.modulesList.stream()
					.map(Module::getModid)
					.collect(Collectors.toList()));
		}
		return super.getTabList(args, sender);
	}
}
