package ru.will0376.windcheck.server.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheckbridge.events.req.RequestClassLoadedListEvent;
import ru.will0376.windcheckbridge.utils.AbstractCommand;
import ru.will0376.windcheckbridge.utils.Token;

import java.util.Arrays;
import java.util.List;

@GradleSideOnly(GradleSide.SERVER)
public class ClassLoadedListCommand extends AbstractCommand {
	public ClassLoadedListCommand() {
		super("clist");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("player").setHasArg()
				.setRequired()
				.build(), getArgBuilder("classLoadersName").build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().adminNick(sender.getName()).args(args).build();

		if (!WindCheck.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}
		CommandLine line = getLine(args);

		if (line.hasOption("player")) {
			String player = line.getOptionValue("player");
			if (WindCheck.aVersion.findPlayer(player) != null) {

				WindCheck.aVersion.createNewToken(line.getOptionValue("player"), sender.getName(),
						sender instanceof MinecraftServer ? Token.Requester.Console : Token.Requester.Admin);
				WindCheck.aVersion.sendEvent(new RequestClassLoadedListEvent(player, sender.getName(),
						sender instanceof MinecraftServer ? Token.Requester.Console : Token.Requester.Admin, line.hasOption(
								"classLoadersName") ? new String[]{
						"classLoadersName"} : null));
			} else {
				return build.setCanceled("player not found");
			}
		}

		return build;
	}

	@Override
	public List<String> getTabList(String[] args, ICommandSender sender) {
		if (args.length == 3 && args[1].contains("player")) {
			return CommandBase.getListOfStringsMatchingLastWord(args, getServer().getOnlinePlayerNames());
		}
		return super.getTabList(args, sender);
	}
}
