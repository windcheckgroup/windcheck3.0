package ru.will0376.windcheck.server.commands;

import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheckbridge.utils.AbstractCommand;

import java.util.Arrays;
import java.util.List;

@GradleSideOnly(GradleSide.SERVER)
public class WebSocketCommand extends AbstractCommand {
	public WebSocketCommand() {
		super("websocket");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("start").build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().adminNick(sender.getName()).args(args).build();
		if (!WindCheck.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);
		if (line.hasOption("start"))
			WindCheck.runWebSocket();


		return build;
	}
}
