package ru.will0376.windcheck.server;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.log4j.Log4j2;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheck.common.ToClientMessage;
import ru.will0376.windcheck.common.templates.*;
import ru.will0376.windcheck.server.utils.ActionLogger;
import ru.will0376.windcheck.server.utils.FileImageLogs;
import ru.will0376.windcheckbridge.events.*;
import ru.will0376.windcheckbridge.events.ans.*;
import ru.will0376.windcheckbridge.events.req.*;
import ru.will0376.windcheckbridge.utils.Module;
import ru.will0376.windcheckbridge.utils.Token;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@GradleSideOnly(GradleSide.SERVER)
public class Events {

	@SubscribeEvent
	public void registerModuleEvent(RegisterModuleEvent event) {
		Module module = event.getModule();
		if (WindCheck.modulesList.contains(module))
			log.error("Module {} from {} already added", module.getModuleName(), module.getModid());
		else {
			WindCheck.aVersion.sendEvent(new AnswerVersionImplEvent(WindCheck.aVersion, module));
			WindCheck.modulesList.add(module);
			log.info("Module {} from {} registered successfully", module.getModuleName(), module.getModid());
		}
	}

	@SubscribeEvent
	public void requestScreenEvent(RequestScreenEvent event) {
		EntityPlayerMP player = WindCheck.aVersion.findPlayer(event.getTargetNick());
		if (player == null) {
			WindCheck.aVersion.printToSender(WindCheck.aVersion.findPlayer(event.getAdminNick(), true), "Player not found");
			return;
		}
		Map<String, String> options = new HashMap<>();
		options.put("adminNick", event.getAdminNick());
		if (ServerConfig.minecraftWindowOnly)
			options.put("minecraftWindowOnly", "");
		if (WindCheck.debug)
			options.put("blackboard", "");
		options.put("port", String.valueOf(ServerConfig.port));
		WindCheck.network.sendTo(new ToClientMessage(ScreenExecutor.class, options), player);
	}

	@SubscribeEvent
	public void parseAnswerFromClientEvent(ParseAnswerFromClientEvent event) {
		JsonObject asJsonObject = new JsonParser().parse(event.getMessage()).getAsJsonObject();
		JsonElement type = asJsonObject.get("type");
		if (type == null) {
			log.error("json from {} haven't needed type", event.getIp());
			return;
		}


		String playerNick = asJsonObject.get("playerNick").getAsString();
		String adminNick = asJsonObject.get("adminNick").getAsString();

		if (WindCheck.checkToken(playerNick, adminNick) == null) {
			log.error("token from {} not found", playerNick);
			return;
		}
		log.debug("Incoming message from {} with type {}", playerNick, type.getAsString());
		switch (type.getAsString()) {
			case "screen":
				WindCheck.aVersion.sendEvent(new AnswerScreenFromClient(adminNick, asJsonObject, playerNick));
				break;

			case "log":
				WindCheck.aVersion.sendEvent(new AnswerLogFromClientEvent(adminNick, asJsonObject, playerNick));
				break;

			case "modlist":
				WindCheck.aVersion.sendEvent(new AnswerModListEvent(adminNick, asJsonObject, playerNick));
				break;

			case "error":
				WindCheck.aVersion.sendEvent(new AnswerErrorFromClientEvent(adminNick, String.format("Error from: %s, message: " +
						"%s", playerNick, asJsonObject.get("text")
						.getAsString()), playerNick));
				break;

			case "classLoadedList":
				WindCheck.aVersion.sendEvent(new AnswerClassLoadedListEvent(adminNick, asJsonObject, playerNick));
				break;

			default:
				WindCheck.aVersion.sendEvent(new ParseJOToAnotherRequester(adminNick, asJsonObject, playerNick));
				break;
		}
	}

	@SubscribeEvent
	public void unpackScreenEvent(AnswerScreenFromClient event) {
		if (!event.isCanceled()) {
			String playerNick = event.getJo().get("playerNick").getAsString();
			String adminNick = event.getJo().get("adminNick").getAsString();
			Token token = WindCheck.checkToken(playerNick, adminNick);
			if (token == null) {
				log.error("Token from {} not found ", playerNick);
				return;
			}
			try {
				token.setDeleted();
				ByteArrayInputStream bis = new ByteArrayInputStream(Base64.getDecoder()
						.decode(event.getJo().get("imgb64").getAsString()));
				BufferedImage image = ImageIO.read(bis);
				bis.close();
				WindCheck.aVersion.sendEvent(new SaveImageEvent(token, image));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	@SubscribeEvent
	public void saveImageEvent(SaveImageEvent event) {
		try {
			Token token = event.getToken();
			String textBack = WindCheck.storage.saveImage(event.getImage(), token.getPlayerNick());
			FileImageLogs.saveToLogger(textBack, token.getAdminNick(), token.getPlayerNick());
			WindCheck.aVersion.sendEvent(new CheckAndPrintEvent(token, textBack, textBack, event.getImage()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SubscribeEvent
	public void printErrorToAdminEvent(AnswerErrorFromClientEvent event) {
		if (!event.isCanceled()) {
			Token token = WindCheck.checkToken(event.getPlayerNick(), event.getAdminNick());
			if (token == null) {
				log.error("Token from {} not found ", event.getPlayerNick());
				return;
			}
			token.setDeleted();
			if (token.getRequester() == Token.Requester.Admin) {
				EntityPlayerMP player = WindCheck.aVersion.findPlayer(event.getAdminNick());
				WindCheck.aVersion.printErrorToSender(player, event.getLink());
			} else if (token.getRequester() == Token.Requester.Console) {
				log.error(event.getLink());
			} else
				WindCheck.aVersion.sendEvent(new PrintInfoForAnotherRequester(token, event.getLink(), null));
		}
	}

	@SubscribeEvent
	public void requestLogEvent(RequestLogFromPlayerEvent event) {
		EntityPlayerMP player = WindCheck.aVersion.findPlayer(event.getTargetNick());
		Map<String, String> options = new HashMap<>();
		options.put("adminNick", event.getAdminNick());
		options.put("http", ServerConfig.hastebinUrl);
		options.put("maxLines", String.valueOf(ServerConfig.maxLines));
		if (event.isDebug())
			options.put("debug", "");
		if (WindCheck.debug)
			options.put("blackboard", "");
		options.put("port", String.valueOf(ServerConfig.port));
		WindCheck.network.sendTo(new ToClientMessage(LogExecutor.class, options), player);
	}

	@SubscribeEvent
	public void requestClassLoadedEvent(RequestClassLoadedListEvent event) {
		EntityPlayerMP player = WindCheck.aVersion.findPlayer(event.getTargetNick());
		Map<String, String> options = new HashMap<>();
		options.put("adminNick", event.getAdminNick());
		options.put("http", ServerConfig.hastebinUrl);
		options.put("maxLines", String.valueOf(ServerConfig.maxLines));
		options.put("port", String.valueOf(ServerConfig.port));
		if (WindCheck.debug)
			options.put("blackboard", "");
		if (event.getOptions() != null) {
			options.put("classLoadersName", "true");
		}
		WindCheck.network.sendTo(new ToClientMessage(ClassLoadedListExecutor.class, options), player);
	}

	@SubscribeEvent
	public void logFromClientEvent(AnswerLogFromClientEvent event) {
		if (!event.isCanceled()) {
			Token token = WindCheck.checkToken(event.getPlayerNick(), event.getAdminNick());
			if (token == null) {
				log.error("Token from {} not found ", event.getPlayerNick());
				return;
			}
			token.setDeleted();
			String text = event.getJo().get("text").getAsString();
			String textBack = String.format("Log link from %s: %s", event.getPlayerNick(), text);
			WindCheck.aVersion.sendEvent(new CheckAndPrintEvent(token, textBack, text));
		}
	}

	@SubscribeEvent
	public void modListFromClientEvent(AnswerModListEvent event) {
		if (!event.isCanceled()) {
			Token token = WindCheck.checkToken(event.getPlayerNick(), event.getAdminNick());
			if (token == null) {
				log.error("Token from {} not found ", event.getPlayerNick());
				return;
			}
			token.setDeleted();
			String text = event.getJo().get("text").getAsString();

			String textBack = String.format("ModList link from %s: %s", event.getPlayerNick(), text);
			WindCheck.aVersion.sendEvent(new CheckAndPrintEvent(token, textBack, text));
		}

	}

	@SubscribeEvent
	public void classLoadedListFromClientEvent(AnswerClassLoadedListEvent event) {
		if (!event.isCanceled()) {
			Token token = WindCheck.checkToken(event.getPlayerNick(), event.getAdminNick());
			if (token == null) {
				log.error("Token from {} not found ", event.getPlayerNick());
				return;
			}
			token.setDeleted();
			String text = event.getJo().get("text").getAsString();

			String textBack = String.format("Class Loaded List link from %s: %s", event.getPlayerNick(), text);
			WindCheck.aVersion.sendEvent(new CheckAndPrintEvent(token, textBack, text));
		}
	}

	@SubscribeEvent
	public void checkAndPrintEvent(CheckAndPrintEvent event) {
		if (event.getToken().getRequester() == Token.Requester.Console) {
			log.info("{}[WindCheck] {} {} -> {}", TextFormatting.GOLD, TextFormatting.RESET, event.getToken()
					.getPlayerNick(), event.getText());
		} else if (event.getToken().getRequester() == Token.Requester.Admin) {
			log.info("{}[WindCheck] {} {} -> {}", TextFormatting.GOLD, TextFormatting.RESET, event.getToken()
					.getPlayerNick(), event.getText());
			EntityPlayerMP player = WindCheck.aVersion.findPlayer(event.getToken().getAdminNick());
			if (player != null)
				WindCheck.aVersion.printToSenderWithClick(player, event.getToken()
						.getPlayerNick() + " -> " + event.getText(), event.getClickText());

		} else {
			WindCheck.aVersion.sendEvent(new PrintInfoForAnotherRequester(event.getToken(), event.getText(), event.getImage()));
		}
	}

	@SubscribeEvent
	public void requestModListEvent(RequestModListEvent event) {
		EntityPlayerMP player = WindCheck.aVersion.findPlayer(event.getTargetNick());
		Map<String, String> options = new HashMap<>();
		options.put("adminNick", event.getAdminNick());
		if (WindCheck.debug)
			options.put("blackboard", "");
		options.put("http", ServerConfig.hastebinUrl);
		WindCheck.network.sendTo(new ToClientMessage(ModListExecutor.class, options), player);
	}

	@SubscribeEvent
	public void requestCrashEvent(RequestCrashPlayerEvent event) {
		WindCheck.network.sendTo(new ToClientMessage(CrashExecutor.class, new HashMap<>()),
				WindCheck.aVersion.findPlayer(event.getTargetNick()));
	}

	@SubscribeEvent
	public void reloadEvent(ReloadEvent event) {
		if (!event.isModuleOnly()) {
			ConfigManager.sync(WindCheck.MOD_ID, Config.Type.INSTANCE);
			log.info("WindCheck reloaded");
			if (!event.getAdminNick().equalsIgnoreCase("console") && !event.getAdminNick().equalsIgnoreCase("rcon"))
				WindCheck.aVersion.senderCanUseCommand(WindCheck.aVersion.findPlayer(event.getAdminNick()), "WindCheck " +
						"reloaded");
		}
	}

	@SubscribeEvent
	public void saveEvent(WorldEvent.Save event) {
		try {
			ActionLogger.save();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
}
