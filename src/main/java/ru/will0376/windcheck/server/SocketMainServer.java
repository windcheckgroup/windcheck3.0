package ru.will0376.windcheck.server;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.log4j.Log4j2;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheckbridge.events.ParseAnswerFromClientEvent;

import java.net.InetSocketAddress;

@GradleSideOnly(GradleSide.SERVER)
@Log4j2
public class SocketMainServer extends WebSocketServer {

	public SocketMainServer(InetSocketAddress address) {
		super(address);
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		String hostAddress = conn.getRemoteSocketAddress().getAddress().getHostAddress();
		if (ServerConfig.getBannedIPs().contains(hostAddress)) {
			log.info("Attempt to access from a blocked ip: {}", hostAddress);
			return;
		}

		if (message == null || message.isEmpty()) {
			log.error("Message from {} is null", hostAddress);
			return;
		}
		try {
			JsonObject asJsonObject = new JsonParser().parse(message).getAsJsonObject();
			if (!asJsonObject.has("type") || !asJsonObject.has("playerNick") || !asJsonObject.has("adminNick")) {
				log.error("Invalid json from {}", hostAddress);
				return;
			}
			log.debug("Message from {} with type {}", hostAddress, asJsonObject.get("type").getAsString());
			WindCheck.aVersion.sendEvent(new ParseAnswerFromClientEvent(message, hostAddress));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		ex.printStackTrace();
	}

	@Override
	public void onStart() {

	}
}

