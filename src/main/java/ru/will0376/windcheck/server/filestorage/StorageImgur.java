package ru.will0376.windcheck.server.filestorage;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.server.ServerConfig;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

@GradleSideOnly(GradleSide.SERVER)
public class StorageImgur extends AbstractStorage implements AbstractStorage.ImageUploader {
	public static final ContentType UTF_8 = ContentType.create("text/plain", Consts.UTF_8);
	private static final Gson gson = new Gson();
	private static final CloseableHttpClient client = HttpClients.custom()
			.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) " + "AppleWebKit/537.36 (KHTML, like Gecko) " + "Chrome/83" +
					".0.4103.109 Safari/537.36")
			.build();
	private static final ResponseHandler<String> defaultResponseHandler = response -> {
		int status = response.getStatusLine().getStatusCode();
		if (status >= 200 && status < 300) {
			HttpEntity entity = response.getEntity();
			return entity != null ? EntityUtils.toString(entity) : null;
		} else {
			HttpEntity entity = response.getEntity();
			String ans = entity != null ? EntityUtils.toString(entity) : null;
			System.out.println(ans);
			throw new ClientProtocolException("Unexpected response status: " + status);
		}
	};

	@Override
	public String saveImage(BufferedImage image, String playerNick) throws Exception {
		return safeUpload(image);
	}

	@Override
	public String uploader(BufferedImage image, String token) throws Exception {
		HttpPost post = new HttpPost("https://api.imgur.com/3/image");
		post.setHeader("accept", "application/json");
		post.setHeader("Authorization", "Client-ID " + token);
		post.setHeader("accept-language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
		post.setHeader("content-type", "application/json");
		post.setHeader("sec-fetch-dest", "empty");
		post.setHeader("sec-fetch-mode", "cors");
		post.setHeader("sec-fetch-site", "same-site");
		ByteArrayOutputStream barray = new ByteArrayOutputStream();
		ImageIO.write(image, "png", barray);
		byte[] bimage = barray.toByteArray();
		String dataImage = (new Base64()).encodeToString(bimage);
		post.setEntity(new StringEntity(dataImage, UTF_8));
		JsonObject json = gson.fromJson(client.execute(post, defaultResponseHandler), JsonObject.class);
		return json.getAsJsonObject("data").get("link").getAsString();
	}

	@Override
	public String safeUpload(BufferedImage image) throws Exception {
		if (ServerConfig.token.isEmpty())
			throw new Exception("Imgur disabled or token invalid");

		return uploader(image, ServerConfig.token);
	}

	@Override
	public String getToken() {
		return "Getting on: https://api.imgur.com/oauth2/addclient Example: https://i.imgur.com/hGlYuuV.png";
	}
}
