package ru.will0376.windcheck.server.filestorage;

import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@GradleSideOnly(GradleSide.SERVER)
public class StorageFile extends AbstractStorage {

	@Override
	public String saveImage(BufferedImage image, String playerNick) throws Exception {
		File folder = new File(WindCheck.rootFolder, "WindCheckImages");

		folder.mkdirs();

		File playerDir = new File(folder, playerNick);
		playerDir.mkdir();

		File savedFile = new File(playerDir, String.format("%s.png", DateTimeFormatter.ofPattern("HH.mm.ss-dd-MM-yyyy")
				.format(LocalDateTime.now())));
		ImageIO.write(image, "png", savedFile);
		return String.format("saved to file: %s/%s", playerNick, savedFile.getName());
	}
}
