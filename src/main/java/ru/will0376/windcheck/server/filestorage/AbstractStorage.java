package ru.will0376.windcheck.server.filestorage;

import lombok.Getter;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheck.server.ServerConfig;

import java.awt.image.BufferedImage;

@GradleSideOnly(GradleSide.SERVER)
public abstract class AbstractStorage {

	public static void setStorage() {
		Storage storage = Storage.valueOf(ServerConfig.storage);
		WindCheck.storage = storage.getStorage();
	}

	public abstract String saveImage(BufferedImage image, String playerNick) throws Exception;

	@Getter
	public enum Storage {
		File(new StorageFile()),
		Imgur(new StorageImgur());

		AbstractStorage storage;

		Storage(AbstractStorage storage) {
			this.storage = storage;
		}
	}

	public interface ImageUploader {
		String uploader(BufferedImage image, String token) throws Exception;

		String safeUpload(BufferedImage image) throws Exception;

		default boolean requireToken() {
			return true;
		}

		String getToken();

		default boolean canCreateAlbums() {
			return false;
		}

	}

}
