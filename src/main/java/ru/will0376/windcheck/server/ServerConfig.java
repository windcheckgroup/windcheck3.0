package ru.will0376.windcheck.server;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheck.server.filestorage.AbstractStorage;

import java.util.Arrays;
import java.util.List;

@GradleSideOnly(GradleSide.SERVER)
@Config(modid = WindCheck.MOD_ID)
@Mod.EventBusSubscriber
public class ServerConfig {

	public static String[] playerWhitelist = new String[]{"Will0376", "Player2"};
	//public static String[] disabledModules = new String[]{""};
	public static String[] bannedIPs = new String[]{};
	@Config.Comment("Catch only minecraft window?")
	public static boolean minecraftWindowOnly = true;
	@Config.Comment({"File", "Imgur(Require Token)"})
	public static String storage = AbstractStorage.Storage.File.name();
	public static String token = "";
	@Config.Comment("HasteBin URL")
	public static String hastebinUrl = "https://log.jocat.ru";
	@Config.Comment("Max lines for hastebin")
	public static int maxLines = 500;
	@Config.Comment("WebSocket Port")
	public static int port = 2015;

	public static List<String> getPlayerWhitelist() {
		return Arrays.asList(playerWhitelist);
	}

	/*public static List<String> getDisabledModules() {
		return Arrays.asList(disabledModules);
	}

	public static void setModifiedDisabledModules(List<String> in) {
		disabledModules = in.toArray(new String[0]);
	}*/
	public static List<String> getBannedIPs() {
		return Arrays.asList(bannedIPs);
	}

	public static void setModifiedBannedIPs(List<String> in) {
		bannedIPs = in.toArray(new String[0]);
	}

	@SubscribeEvent
	public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
		if (event.getModID().equals(WindCheck.MOD_ID)) {
			ConfigManager.sync(WindCheck.MOD_ID, Config.Type.INSTANCE);
		}
	}

}
