package ru.will0376.windcheck.server.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import lombok.extern.log4j.Log4j2;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheckbridge.utils.AbstractCommand;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@GradleSideOnly(GradleSide.SERVER)
public class ActionLogger {
	public static List<AbstractCommand.ActionStructure> list = new ArrayList<>();
	public static File logDir, fileNow;
	public static SimpleDateFormat format = new SimpleDateFormat("dd-MM--HH-mm-ss");
	public static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

	public static void init() {
		logDir.mkdir();
		fileNow = new File(logDir, WindCheck.debug ? "debug.json" : format.format(System.currentTimeMillis()) + ".json");
	}

	public static void save() {
		try {
			if (!list.isEmpty()) {
				fileNow.createNewFile();

				String toJson = gson.toJson(list, new TypeToken<List<AbstractCommand.ActionStructure>>() {
				}.getType());

				BufferedWriter writer = new BufferedWriter(new FileWriter(fileNow));
				writer.write(toJson);
				writer.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void addAction(AbstractCommand.ActionStructure structure) {
		list.add(structure);
	}
}
