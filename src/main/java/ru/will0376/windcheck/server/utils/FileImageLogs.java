package ru.will0376.windcheck.server.utils;

import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@GradleSideOnly(GradleSide.SERVER)
public class FileImageLogs {
	public static File pathToLogsFolder = null;

	public FileImageLogs() {
		File dir = new File(WindCheck.rootFolder, "WindLogs");
		if (!dir.exists())
			dir.mkdir();
		pathToLogsFolder = dir;
	}

	private static String readUsingFiles(String fileName) throws IOException {
		return new String(Files.readAllBytes(Paths.get(fileName)));
	}

	public static String getLogsByNick(String nick) throws IOException {
		File NickLog = createFileLogFromNick(nick);
		return readUsingFiles(NickLog.getAbsolutePath());
	}

	public static void saveToLogger(String url, String sender, String nick) {
		File NickLog = createFileLogFromNick(nick);
		String tmp = logFormation(url, sender);
		writeToFile(NickLog, tmp);
	}

	private static void writeToFile(File nickLog, String tmp) {
		try {
			RandomAccessFile f = new RandomAccessFile(nickLog, "rw");
			f.seek(0); // to the beginning
			f.write((tmp + "\n" + readUsingFiles(nickLog.getAbsolutePath())).getBytes());
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static File createFileLogFromNick(String nick) {
		File file = new File(pathToLogsFolder.getAbsolutePath(), nick + ".log");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file;
	}

	private static String logFormation(String url, String sender) {
		//Time;who;url
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
		Date date = new Date();
		return dateFormat.format(date) + " ; " + sender + " ; " + url;
	}
}

