package ru.will0376.windcheck;

import lombok.extern.log4j.Log4j2;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.FMLLaunchHandler;
import net.minecraftforge.fml.relauncher.Side;
import org.java_websocket.server.WebSocketServer;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.windcheck.common.ToClientMessage;
import ru.will0376.windcheck.server.Events;
import ru.will0376.windcheck.server.ServerConfig;
import ru.will0376.windcheck.server.SocketMainServer;
import ru.will0376.windcheck.server.commands.CommandMain;
import ru.will0376.windcheck.server.filestorage.AbstractStorage;
import ru.will0376.windcheck.server.utils.ActionLogger;
import ru.will0376.windcheck.server.utils.FileImageLogs;
import ru.will0376.windcheckbridge.utils.AVersion;
import ru.will0376.windcheckbridge.utils.Module;
import ru.will0376.windcheckbridge.utils.Token;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Mod(modid = WindCheck.MOD_ID,
		name = WindCheck.MOD_NAME,
		version = WindCheck.VERSION)
@Log4j2
public class WindCheck {

	public static final String MOD_ID = "windcheck";
	public static final String MOD_NAME = "WindCheck";
	public static final String VERSION = "@version@";

	public static boolean debug = true;
	public static SimpleNetworkWrapper network;
	public static String projectName = "empty";

	@GradleSideOnly(GradleSide.SERVER)
	public static List<Token> tokenList;
	@GradleSideOnly(GradleSide.SERVER)
	public static List<Module> modulesList;
	@GradleSideOnly(GradleSide.SERVER)
	public static AVersion aVersion;
	@GradleSideOnly(GradleSide.SERVER)
	public static String channelName;
	@GradleSideOnly(GradleSide.SERVER)
	public static WebSocketServer webSocket;
	@GradleSideOnly(GradleSide.SERVER)
	public static AbstractStorage storage;
	@GradleSideOnly(GradleSide.SERVER)
	public static File rootFolder;
	@GradleSideOnly(GradleSide.SERVER)
	public static Module module;

	@Mod.Instance(MOD_ID)
	public static WindCheck INSTANCE;

	static {
		Invoke.server(() -> {
			tokenList = new ArrayList<>();
			modulesList = new ArrayList<Module>() {{
				add(Module.builder().setDefaultNames("all").modid("all").version("all").build());
			}};
			aVersion = new AVersionImpl();
			module = Module.builder().setDefaultNames("main").modid(MOD_ID).version(projectName).build();
		});
	}

	@GradleSideOnly(GradleSide.SERVER)
	public static Module findByModid(String in) {
		for (Module module : modulesList) {
			if (module.getModid().equals(in))
				return module;
		}
		return null;
	}

	@GradleSideOnly(GradleSide.SERVER)
	public static Token checkToken(String playerNick, String adminNick) {
		for (Token token : tokenList) {
			if (token.getPlayerNick().equalsIgnoreCase(playerNick) && token.getAdminNick().equalsIgnoreCase(adminNick)) {
				return token;
			}
		}
		return null;
	}

	@GradleSideOnly(GradleSide.SERVER)
	private static String random(int x) {
		if (debug)
			return "debug";
		Random r = new Random();

		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < x; i++) {
			builder.append(alphabet.charAt(r.nextInt(alphabet.length())));
		}
		return builder.toString();
	}

	@GradleSideOnly(GradleSide.SERVER)
	public static void runWebSocket() {
		try {
			webSocket = new SocketMainServer(new InetSocketAddress("0.0.0.0", ServerConfig.port));
			webSocket.start();
			log.info("Started WebSocket on 0.0.0.0:{}", ServerConfig.port);
		} catch (Exception e) {
			e.printStackTrace();
			FMLCommonHandler.instance().getMinecraftServerInstance().initiateShutdown();
		}
	}

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		if (debug)
			debug = FMLLaunchHandler.isDeobfuscatedEnvironment();
		Invoke.server(() -> {
			log.info("Loading WindCheck ver:{} for project {} by Will0376", VERSION, projectName);
			AbstractStorage.setStorage();

			rootFolder = new File(event.getModConfigurationDirectory().getParent(), "WindCheck");
			rootFolder.mkdir();
			ActionLogger.logDir = new File(rootFolder, "ActLogger");
			ActionLogger.init();
			new FileImageLogs();

			network = new SimpleNetworkWrapper(channelName = random(8));
			network.registerMessage(new ToClientMessage(), ToClientMessage.class, 0, Side.CLIENT);

			MinecraftForge.EVENT_BUS.register(new Events());
			aVersion.registerModule(module);
		});
	}

	@Mod.EventHandler
	@GradleSideOnly(GradleSide.SERVER)
	public void serverStartedEvent(FMLServerStartingEvent event) {
		runWebSocket();
		event.registerServerCommand(new CommandMain());
	}

	@Mod.EventHandler
	@GradleSideOnly(GradleSide.SERVER)
	public void serverStoppedEvent(FMLServerStoppingEvent event) {
		try {
			webSocket.stop(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
