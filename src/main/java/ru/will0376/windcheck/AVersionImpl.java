package ru.will0376.windcheck;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.Event;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.server.ServerConfig;
import ru.will0376.windcheckbridge.events.RegisterModuleEvent;
import ru.will0376.windcheckbridge.utils.*;

import java.io.File;
import java.util.Arrays;

@GradleSideOnly(GradleSide.SERVER)
public class AVersionImpl extends AVersion {

	@Override
	public void printErrorToSender(ICommandSender sender, String text) {
		sender.sendMessage(new TextComponentString(TextFormatting.GOLD + "[WindCheck] " + TextFormatting.RED + text));
	}

	@Override
	public void printToSender(ICommandSender sender, String text) {
		sender.sendMessage(new TextComponentString(TextFormatting.GOLD + "[WindCheck] " + TextFormatting.RESET + text));
	}

	@Override
	public void printToSenderWithClick(ICommandSender sender, String text, String textClick) {
		TextComponentString component =
				new TextComponentString(TextFormatting.GOLD + "[ClientTweaker] " + TextFormatting.RESET + text);
		component.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, textClick));
		sender.sendMessage(component);
	}

	@Override
	public void printToSenderWithCase(ICommandSender sender, String text) {
		printToSender(sender, text);
	}

	@Override
	public boolean senderCanUseCommand(ICommandSender sender, String text) {
		if (sender == null)
			return false;
		return sender.canUseCommand(4, AbstractCommand.getPermissionPrefix() + text);
	}

	@Override
	public SubCommands registerNewCommand(AbstractCommand AbstractCommand) {
		return EnumHelper.addEnum(SubCommands.class, AbstractCommand.getCommandName(), new Class<?>[]{
				AbstractCommand.class}, AbstractCommand);
	}

	@Override
	public Token.Requester registerModule(Module module) {
		sendEvent(new RegisterModuleEvent(module));
		return EnumHelper.addEnum(Token.Requester.class, module.getModuleName(), new Class<?>[]{});
	}

	@Override
	public <T extends Event> boolean sendEvent(T event) {
		return MinecraftForge.EVENT_BUS.post(event);
	}

	@Override
	public String getRandomKey() {
		return WindCheck.MOD_NAME;
	}

	@Override
	public EntityPlayerMP findPlayer(String in) {
		return findPlayer(in, false);
	}

	@Override
	public EntityPlayerMP findPlayer(String in, boolean bool) {
		if (!checkPlayer(in, bool))
			return null;

		return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(in);
	}

	@Override
	public boolean checkPlayer(String in, boolean bool) {
		return !ServerConfig.getPlayerWhitelist().contains(in) || bool;
	}

	@Override
	public Token createNewToken(String playerNick, String adminNick, Token.Requester requester) {
		Token token = Token.builder()
				.playerNick(playerNick)
				.adminNick(adminNick)
				.requester(requester)
				.build()
				.generateAndSetB64Token();
		WindCheck.tokenList.add(token);
		return token;
	}

	@Override
	public File getDefaultDir() {
		return WindCheck.rootFolder;
	}

	@Override
	public boolean isModuleLoaded(String moduleModId) {
		return WindCheck.modulesList.stream().anyMatch(e -> e.getModid().equals(moduleModId));
	}

	@Override
	public Module getLoadedModule(String moduleModId) {
		return WindCheck.findByModid(moduleModId);
	}

	@Override
	public Module getModuleModidAll() {
		return WindCheck.modulesList.get(0);
	}

	@Override
	public Token.Requester registerNewRequester(String name) {
		return EnumHelper.addEnum(Token.Requester.class, name, new Class<?>[]{});
	}

	@Override
	public boolean isRequesterExist(String name) {
		return Arrays.stream(Token.Requester.values()).anyMatch(e -> e.name().equals(name));
	}
}
