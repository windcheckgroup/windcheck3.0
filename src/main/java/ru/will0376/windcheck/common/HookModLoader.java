package ru.will0376.windcheck.common;

import ru.will0376.windcheck.hooklib.minecraft.HookLoader;
import ru.will0376.windcheck.hooklib.minecraft.PrimaryClassTransformer;

public class HookModLoader extends HookLoader {
	@Override
	public String[] getASMTransformerClass() {
		return new String[]{PrimaryClassTransformer.class.getName()};
	}

	@Override
	protected void registerHooks() {
		registerHookContainer(Hooks.class.getName());
	}
}
