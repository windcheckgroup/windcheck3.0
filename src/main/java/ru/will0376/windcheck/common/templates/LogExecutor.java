package ru.will0376.windcheck.common.templates;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Loader;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.hooklib.minecraft.IExecutor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

@GradleSideOnly(GradleSide.SERVER)
public class LogExecutor implements IExecutor {

	private final ResponseHandler<String> defaultResponseHandler = response -> {
		int status = response.getStatusLine().getStatusCode();
		if (status >= 200 && status < 300) {
			HttpEntity entity = response.getEntity();
			return entity != null ? EntityUtils.toString(entity) : null;
		} else {
			throw new ClientProtocolException("Unexpected response status: " + status);
		}
	};

	@Override
	public void execute() {
		try {
			File logFile = new File(new File(Loader.instance()
					.getConfigDir()
					.getParent()), options.containsKey("debug") ? "logs/debug.log" : "logs/latest.log");

			String post = post(tail(logFile, Integer.parseInt(options.get("maxLines"))), options.get("http"));

			JsonObject jo = new JsonObject();
			jo.addProperty("type", "log");
			jo.addProperty("text", post);
			jo.addProperty("playerNick", Minecraft.getMinecraft().player.getName());
			jo.addProperty("adminNick", options.get("adminNick"));
			createAnswer(jo);
		} catch (Throwable exception) {
			JsonObject jo = new JsonObject();
			jo.addProperty("type", "error");
			jo.addProperty("text", exception.getMessage());
			jo.addProperty("playerNick", Minecraft.getMinecraft().player.getName());
			jo.addProperty("adminNick", options.get("adminNick"));
			createAnswer(jo);
			exception.printStackTrace();
		}
	}

	public String post(String text, String http) throws Throwable {
		HttpPost post = new HttpPost(http + "/documents");
		post.setEntity(new StringEntity(text, ContentType.create("text/plain", Consts.UTF_8)));
		JsonObject json = new Gson().fromJson(HttpClients.custom()
				.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103" +
						".109 Safari/537.36")
				.build()
				.execute(post, defaultResponseHandler), JsonObject.class);

		return String.format("%s/%s", http, json.get("key").getAsString());
	}

	public String tail(File src, int maxLines) throws Throwable {
		BufferedReader reader = new BufferedReader(new FileReader(src));
		String[] lines = new String[maxLines];
		int lastNdx = 0;
		for (String line = reader.readLine(); line != null; line = reader.readLine()) {
			if (lastNdx == lines.length) {
				lastNdx = 0;
			}
			lines[lastNdx++] = line;
		}

		StringBuilder builder = new StringBuilder();
		for (int ndx = lastNdx; ndx != lastNdx - 1; ndx++) {
			if (ndx == lines.length) {
				ndx = 0;
			}
			String line = lines[ndx];
			if (line == null)
				continue;

			builder.append(line).append("\n");
		}

		return builder.toString();
	}
}
