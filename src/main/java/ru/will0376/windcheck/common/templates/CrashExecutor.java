package ru.will0376.windcheck.common.templates;

import net.minecraft.client.Minecraft;
import net.minecraft.crash.CrashReport;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.hooklib.minecraft.IExecutor;

@GradleSideOnly(GradleSide.SERVER)
public class CrashExecutor implements IExecutor {
	@Override
	public void execute() {
		Minecraft.getMinecraft().crashed(new CrashReport("Player not found", new NullPointerException()));
	}
}
