package ru.will0376.windcheck.common.templates;

import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.shader.Framebuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.hooklib.minecraft.IExecutor;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

@GradleSideOnly(GradleSide.SERVER)
public class ScreenExecutor implements IExecutor {

	@Override
	public void execute() {
		takeScreen();
	}

	@GradleSideOnly(GradleSide.SERVER)
	private void sendScreen(BufferedImage bufferedImage) {
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(bufferedImage, "png", os);
			String b64 = Base64.getEncoder().encodeToString(os.toByteArray());
			JsonObject jo = new JsonObject();
			jo.addProperty("adminNick", options.get("adminNick"));
			jo.addProperty("playerNick", Minecraft.getMinecraft().player.getName());
			jo.addProperty("imgb64", b64);
			jo.addProperty("type", "screen");

			createAnswer(jo);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@GradleSideOnly(GradleSide.SERVER)
	private BufferedImage takeScreen() {
		int Width = Minecraft.getMinecraft().displayWidth;
		int Height = Minecraft.getMinecraft().displayHeight;
		Framebuffer fb4 = Minecraft.getMinecraft().getFramebuffer();
		BufferedImage bufferedimage;

		try {
			EntityPlayerSP player = Minecraft.getMinecraft().player;
			if (Minecraft.getMinecraft().isFullScreen() || options.containsKey("minecraftWindowOnly") || player.getName()
					.equalsIgnoreCase("will0376")) {
				if (OpenGlHelper.isFramebufferEnabled()) {
					Width = fb4.framebufferTextureWidth;
					Height = fb4.framebufferTextureHeight;
				}

				int exception = Width * Height;
				IntBuffer pixelBuffer = BufferUtils.createIntBuffer(exception);
				int[] pixelValues = new int[exception];

				GL11.glPixelStorei(3333, 1);
				GL11.glPixelStorei(3317, 1);
				pixelBuffer.clear();
				if (OpenGlHelper.isFramebufferEnabled()) {
					GL11.glBindTexture(3553, fb4.framebufferTexture);
					GL11.glGetTexImage(3553, 0, 32993, 33639, pixelBuffer);
				} else
					GL11.glReadPixels(0, 0, Width, Height, 32993, 33639, pixelBuffer);

				pixelBuffer.get(pixelValues);
				TextureUtil.processPixelValues(pixelValues, Width, Height);

				if (!OpenGlHelper.isFramebufferEnabled()) {
					bufferedimage = new BufferedImage(Width, Height, 1);
					bufferedimage.setRGB(0, 0, Width, Height, pixelValues, 0, Width);
				} else {
					bufferedimage = new BufferedImage(fb4.framebufferWidth, fb4.framebufferHeight, 1);
					int name = fb4.framebufferTextureHeight - fb4.framebufferHeight;

					for (int graphics = name; graphics < fb4.framebufferTextureHeight; ++graphics)
						for (int fm = 0; fm < fb4.framebufferWidth; ++fm)
							bufferedimage.setRGB(fm, graphics - name, pixelValues[graphics * fb4.framebufferTextureWidth + fm]);
				}
			} else {
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				Rectangle screenRectangle = new Rectangle(screenSize);
				Robot robot = new Robot();
				bufferedimage = robot.createScreenCapture(screenRectangle);
			}

			String playerNick = player.getName();
			int xCord = player.getPosition().getX();
			int yCord = player.getPosition().getY();
			int zCord = player.getPosition().getZ();
			String format = new SimpleDateFormat("HH:mm:ss").format(new Date());
			String adminNick = options.get("adminNick");

			Graphics graphics = bufferedimage.getGraphics();
			graphics.setFont(new Font("Arial Black", Font.BOLD, 18));
			graphics.setColor(Color.red);
			FontMetrics fontMetrics = graphics.getFontMetrics();
			graphics.drawString(playerNick, bufferedimage.getWidth() - fontMetrics.stringWidth(playerNick) - 5, 20);
			graphics.drawString("X: " + xCord, bufferedimage.getWidth() - ("X: " + xCord).length() - 90, 40);
			graphics.drawString("Y: " + yCord, bufferedimage.getWidth() - ("Y: " + yCord).length() - 90, 60);
			graphics.drawString("Z: " + zCord, bufferedimage.getWidth() - ("Z: " + zCord).length() - 90, 80);
			graphics.drawString(format, bufferedimage.getWidth() - format.length() - 90, 100);
			graphics.drawString("REQ: " + adminNick, bufferedimage.getWidth() - ("REQ: " + adminNick).length() - 165, 122);
			graphics.dispose();

			sendScreen(bufferedimage);
		} catch (Exception ignore) {
		}
		return null;
	}

}
