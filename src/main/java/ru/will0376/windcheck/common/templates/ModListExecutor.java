package ru.will0376.windcheck.common.templates;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.hooklib.minecraft.IExecutor;

@GradleSideOnly(GradleSide.SERVER)
public class ModListExecutor implements IExecutor {
	private final ResponseHandler<String> defaultResponseHandler = response -> {
		int status = response.getStatusLine().getStatusCode();
		if (status >= 200 && status < 300) {
			HttpEntity entity = response.getEntity();
			return entity != null ? EntityUtils.toString(entity) : null;
		} else {
			throw new ClientProtocolException("Unexpected response status: " + status);
		}
	};

	public String post(String text, String http) throws Throwable {
		HttpPost post = new HttpPost(http + "/documents");
		post.setEntity(new StringEntity(text, ContentType.create("text/plain", Consts.UTF_8)));
		JsonObject json = new Gson().fromJson(HttpClients.custom()
				.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103" +
						".109 Safari/537.36")
				.build()
				.execute(post, defaultResponseHandler), JsonObject.class);

		return String.format("%s/%s", http, json.get("key").getAsString());
	}

	@Override
	public void execute() {
		try {
			StringBuilder builder = new StringBuilder();
			for (ModContainer modContainer : Loader.instance().getActiveModList()) {
				builder.append(String.format("modid: %s, name: %s, ver: %s, jar: %s", modContainer.getModId(),
						modContainer.getName(), modContainer.getVersion(), modContainer.getSource()
						.getName())).append("\n");
			}

			String post = post(builder.toString(), options.get("http"));

			JsonObject jo = new JsonObject();
			jo.addProperty("type", "modlist");
			jo.addProperty("text", post);
			jo.addProperty("playerNick", Minecraft.getMinecraft().player.getName());
			jo.addProperty("adminNick", options.get("adminNick"));
			createAnswer(jo);
		} catch (Throwable exception) {
			JsonObject jo = new JsonObject();
			jo.addProperty("type", "error");
			jo.addProperty("text", exception.getMessage());
			jo.addProperty("playerNick", Minecraft.getMinecraft().player.getName());
			jo.addProperty("adminNick", options.get("adminNick"));
			createAnswer(jo);
			exception.printStackTrace();
		}
	}
}
