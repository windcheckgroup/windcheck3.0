package ru.will0376.windcheck.common.templates;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.hooklib.minecraft.IExecutor;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Vector;

@GradleSideOnly(GradleSide.SERVER)
public class ClassLoadedListExecutor implements IExecutor {
	private final ResponseHandler<String> defaultResponseHandler = response -> {
		int status = response.getStatusLine().getStatusCode();
		if (status >= 200 && status < 300) {
			HttpEntity entity = response.getEntity();
			return entity != null ? EntityUtils.toString(entity) : null;
		} else {
			throw new ClientProtocolException("Unexpected response status: " + status);
		}
	};

	private static Iterator<?> getList(ClassLoader CL) throws NoSuchFieldException, SecurityException, IllegalArgumentException,
			IllegalAccessException {
		Class<?> CL_class = CL.getClass();
		while (CL_class != ClassLoader.class) {
			CL_class = CL_class.getSuperclass();
		}

		Field ClassLoader_classes_field = CL_class.getDeclaredField("classes");
		ClassLoader_classes_field.setAccessible(true);
		Vector classes = (Vector) ClassLoader_classes_field.get(CL);
		return classes.iterator();
	}

	public String post(String text, String http) throws Throwable {
		HttpPost post = new HttpPost(http + "/documents");
		post.setEntity(new StringEntity(text, ContentType.create("text/plain", Consts.UTF_8)));
		JsonObject json = new Gson().fromJson(HttpClients.custom()
				.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103" +
						".109 Safari/537.36")
				.build()
				.execute(post, defaultResponseHandler), JsonObject.class);

		return String.format("%s/%s", http, json.get("key").getAsString());
	}

	@Override
	public void execute() {
		try {
			Iterator<?> list = getList(getFirstClassLoader(Minecraft.getMinecraft().getClass().getClassLoader()));
			StringBuilder builder = new StringBuilder();
			while (list.hasNext()) {
				Object next = list.next();
				if (options.containsKey("classLoadersName"))
					builder.append(((Class<?>) next).getClassLoader().getClass().getSimpleName()).append(" => ");
				if (next instanceof Class)
					builder.append(((Class<?>) next).getName());
				else
					builder.append(next).append(" not a class!!");
				builder.append("\n");
			}
			String http = post(builder.toString(), options.get("http"));

			JsonObject jo = new JsonObject();
			jo.addProperty("type", "classLoadedList");
			jo.addProperty("text", http);
			jo.addProperty("playerNick", Minecraft.getMinecraft().player.getName());
			jo.addProperty("adminNick", options.get("adminNick"));
			createAnswer(jo);
		} catch (Throwable ex) {
			JsonObject jo = new JsonObject();
			jo.addProperty("type", "error");
			jo.addProperty("text", ex.getMessage());
			jo.addProperty("playerNick", Minecraft.getMinecraft().player.getName());
			jo.addProperty("adminNick", options.get("adminNick"));
			createAnswer(jo);
			ex.printStackTrace();
		}
	}

	private ClassLoader getFirstClassLoader(ClassLoader classLoader) {
		ClassLoader lastLoader = classLoader;
		while (true) {
			ClassLoader parent = lastLoader.getParent();
			if (parent != null)
				lastLoader = parent;
			else
				return lastLoader;
		}
	}
}
