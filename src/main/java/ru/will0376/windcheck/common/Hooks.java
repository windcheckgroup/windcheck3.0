package ru.will0376.windcheck.common;

import net.minecraft.network.PacketBuffer;
import net.minecraft.network.login.server.SPacketLoginSuccess;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheck.hooklib.asm.Hook;

public class Hooks {

	@GradleSideOnly(GradleSide.SERVER)
	@SideOnly(Side.SERVER)
	@Hook(targetMethod = "writePacketData",
			injectOnExit = true)
	public static void writePacketData(SPacketLoginSuccess packet, PacketBuffer buf) {
		buf.writeString(WindCheck.channelName);
	}

	@SideOnly(Side.CLIENT)
	@Hook(targetMethod = "readPacketData",
			injectOnExit = true)
	public static void readPacketData(SPacketLoginSuccess packet, PacketBuffer buf) {
		try {
			String tmp = buf.readString(8);
			WindCheck.network = new SimpleNetworkWrapper(tmp);
			WindCheck.network.registerMessage(new ToClientMessage(), ToClientMessage.class, 0, Side.CLIENT);
		} catch (Exception ignore) {
		}
	}
}
