package ru.will0376.windcheck.common;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import org.apache.commons.io.IOUtils;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.windcheck.WindCheck;
import ru.will0376.windcheck.client.ByteClassLoader;
import ru.will0376.windcheck.hooklib.minecraft.IExecutor;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Getter
public class ToClientMessage implements IMessage, IMessageHandler<ToClientMessage, IMessage> {
	Class<? extends IExecutor> executor;
	Map<String, String> options = new HashMap<>();
	boolean error;


	public ToClientMessage(Class<? extends IExecutor> executorClazz, Map<String, String> options) {
		this.executor = executorClazz;
		this.options = options;
	}

	@Override
	public void fromBytes(ByteBuf buf) {

		try {
			ByteClassLoader byteClassLoader = new ByteClassLoader(WindCheck.INSTANCE.getClass().getClassLoader());

			executor = (Class<? extends IExecutor>) byteClassLoader.loadClass(loadClass(buf, byteClassLoader));

			int clazzCount = buf.readInt();

			if (clazzCount > 0) {
				byteClassLoader.loadClass(loadClass(buf, byteClassLoader));
			}

			int mapSize = buf.readInt();
			for (int i = 0; i < mapSize; i++) {
				String key = ByteBufUtils.readUTF8String(buf);
				String value = ByteBufUtils.readUTF8String(buf);
				options.put(key, value);
			}

			error = buf.readBoolean();
		} catch (Exception ex) {
			ex.printStackTrace();
			error = true;
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		Invoke.server(() -> serverWrite(buf));
	}

	public String loadClass(ByteBuf buf, ByteClassLoader byteClassLoader) {
		byte[] classByte = Base64.getDecoder().decode(ByteBufUtils.readUTF8String(buf).getBytes(StandardCharsets.UTF_8));

		String classFullName = ByteBufUtils.readUTF8String(buf);

		byteClassLoader.loadDataInBytes(classByte, classFullName);
		return classFullName;
	}

	@GradleSideOnly(GradleSide.SERVER)
	private void serverWrite(ByteBuf buf) {
		try {
			writeClass(buf, executor);
			{
				IExecutor iExecutor = executor.newInstance();
				buf.writeInt(iExecutor.getInnerClassesCount());

				if (iExecutor.getInnerClassesCount() > 0) {
					for (String innerClassesName : iExecutor.getInnerClassesNames()) {
						Class<?> aClass = Class.forName(String.format("%s$%s", executor.getName(), innerClassesName));
						writeClass(buf, aClass);
					}
				}
			}

			buf.writeInt(options.size());

			options.forEach((s, s2) -> {
				ByteBufUtils.writeUTF8String(buf, s);
				ByteBufUtils.writeUTF8String(buf, s2);
			});

			buf.writeBoolean(false);
		} catch (Exception ex) {
			ex.printStackTrace();
			buf.writeBoolean(true);
		}
	}

	@GradleSideOnly(GradleSide.SERVER)
	private void writeClass(ByteBuf buf, Class<?> clazz) throws Exception {
		String className = clazz.getName();
		String classAsPath = className.replace('.', '/') + /*(!FMLLaunchHandler.isDeobfuscatedEnvironment() ? ".class" : "
		.java")*/ ".class";
		InputStream stream = WindCheck.INSTANCE.getClass().getClassLoader().getResourceAsStream(classAsPath);

		byte[] bytes = IOUtils.toByteArray(stream);
		ByteBufUtils.writeUTF8String(buf, new String(Base64.getEncoder().encode(bytes)));
		ByteBufUtils.writeUTF8String(buf, className);
	}

	@Override
	public IMessage onMessage(ToClientMessage message, MessageContext ctx) {
		try {
			if (message.isError()) {
				System.err.println("Error in Packet");
				return null;
			}
			Minecraft.getMinecraft().addScheduledTask(() -> {
				try {
					message.getExecutor().newInstance().setOptions(message.options).execute();
				} catch (InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
