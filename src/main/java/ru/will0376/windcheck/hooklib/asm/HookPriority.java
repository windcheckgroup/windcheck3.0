package ru.will0376.windcheck.hooklib.asm;

public enum HookPriority {

	HIGHEST, // Вызывается первым
	HIGH,
	NORMAL,
	LOW,
	LOWEST // Вызывается последним

}
