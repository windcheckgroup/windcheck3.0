package ru.will0376.windcheck.hooklib.minecraft;

import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import org.java_websocket.client.WebSocketClient;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public interface IExecutor {
	Map<String, String> options = new HashMap<>();

	void execute();

	default IExecutor setOptions(Map<String, String> in) {
		options.putAll(in);
		return this;
	}

	default int getInnerClassesCount() {
		return getInnerClassesNames().length;
	}

	default String[] getInnerClassesNames() {
		return new String[]{};
	}

	default void createAnswer(JsonObject jo) {
		new Thread(() -> {
			try {
				String ip = "localhost";
				if (Minecraft.getMinecraft().getConnection() != null && !options.containsKey("blackboard"))
					ip = Minecraft.getMinecraft()
							.getConnection()
							.getNetworkManager()
							.getRemoteAddress()
							.toString()
							.split("/")[1].split(":")[0];

				WebSocketClient wsc = new WebSocketClientImpl(URI.create("ws://" + ip + ":" + options.get("port")));
				wsc.connect();
				while (!wsc.isOpen()) {
				}
				wsc.send(jo.toString());
				wsc.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}
}
